<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class UserController extends Controller {
    /**
     * @OA\Get(
     *     path="/users",
     *     summary="Get all users.",
     *     tags={"All", "Users"},
     *     operationId="getUsers",
     *   @OA\Response(
     *     response=200,
     *     description="Data",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *      ),
     *     ),
     *     @OA\Schema(
     *       type="array",
     *       @OA\Items(
     *        @OA\Property(property="ID", type="integer"),
     *        @OA\Property(property="USERNAME", type="string"),
     *        @OA\Property(property="EMAIL", type="string")
     *       )
     *     )
     *   )
     * )
     */

    public function get_users() {
        $users = array();
        $result = DB::table('user_test')->get();
        foreach($result as $user) {
            $encoded_users = array('ID' => $user->id, 'USERNAME' => $user->username, 'EMAIL'=> $user->email);
            array_push($users, $encoded_users);
        }
        return $users;
    }


    /**
     * @OA\Get(
     *     path="/users/{id}",
     *     summary="Get specific user.",
     *     tags={"All", "Users"},
     *     operationId="getUser",
     *   @OA\Parameter(
     *     in="path",
     *     name="id",
     *     required=true,
     *     @OA\Schema(
     *          type="string"
     *     ),
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="Data",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *      ),
     *     @OA\Schema(
     *       type="array",
     *       @OA\Items(
     *        @OA\Property(property="ID", type="integer"),
     *        @OA\Property(property="USERNAME", type="string"),
     *        @OA\Property(property="EMAIL", type="string")
     *       )
     *     )
     *   )
     * )
     */
    public function get_user(Request $request) {
        $user = array();
        $userId = $request->route('id');
        $result = DB::select('select * from user_test where ID = :id', ['id' => $userId]);
        foreach($result as $user) {
            $encoded_user = array('ID' => $user->id, 'USERNAME' => $user->username, 'EMAIL'=> $user->email);
            $user = $encoded_user;
        }
        return $user;
    }


    /**
     * @OA\Post(
     *     path="/users",
     *     summary="Create new user",
     *     tags={"All", "User"},
     *     operationId="createUser",
     *   @OA\Parameter(
     *     in="query",
     *     name="User",
     *     description="Description of new user",
     *     required=true,
     *     @OA\Schema(
     *      @OA\Property(property="USERNAME", type="string"),
     *      @OA\Property(property="EMAIL", type="string"),
     *     )
     *   ),
     *   @OA\Response(
     *     response=200,
     *     description="User Created",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *      )
     *   )
     * )
     */
    public function create_user(Request $request) {
        $username = $request->input('USERNAME');
        $email = $request->input('EMAIL');
        $userId = DB::table('user_test')->insertGetId(['username' => $username, 'email' => $email]);
        return $userId;
    }

    /**
     * @OA\Put(
     *     path="/users/{id}",
     *     summary="Update user.",
     *     tags={"All", "User"},
     *     operationId="updateUser",
     *   @OA\Parameter(
     *     in="path",
     *     name="id",
     *     required=true,
     *     @OA\Schema(
     *          type="integer",
     *          format="int64",
     *          description="The unique identifier of a request",
     *     ),
     *   ),
     *     @OA\Response(
     *         response=204,
     *         description="Successfully updated user.",
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="Invalid id input"
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="User not found"
     *     ),
     * )
     */

    public function update_user(Request $request) {
        $userId = $request->route('id');
        $username = $request->input('USERNAME');
        $email = $request->input('EMAIL');
        $affected = DB::update('update user_test set username = :username, email =:email where id =:id', ['username' => $username, 'email' => $email, 'id' => $userId]);
    }

    /**
     * @OA\Delete(
     *     path="/users/{id}",
     *     summary="Delete user.",
     *     tags={"All", "Requests"},
     *     operationId="deleteUser",
     *   @OA\Parameter(
     *     in="path",
     *     name="id",
     *     required=true,
     *     @OA\Schema(
     *          type="integer",
     *          format="int64",
     *          description="The unique identifier of a user",
     *     ),
     *   ),
     *     @OA\Response(
     *         response=204,
     *         description="Successfully deleted user.",
     *     ),
     *     @OA\Response(
     *          response=400,
     *          description="Invalid id input"
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="User not found"
     *     ),
     * )
     */

    public function delete_user(Request $request) {
        $userId = $request->route('id');
        $affected = DB::delete('delete from user_test where id = :id', ['id' => $userId]);
    }
}$