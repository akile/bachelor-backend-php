<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return phpinfo();
});

$router->get('/users', 'UserController@get_users');

$router->post('/users', 'UserController@create_user');

$router->get('/users/{id}', 'UserController@get_user');

$router->put('/users/{id}', 'UserController@update_user');

$router->delete('/users/{id}', 'UserController@delete_user');

$router->get('users/test', function () {
    require("vendor/autoload.php");
    $openapi = \OpenApi\scan('/Users/akile/OneDrive/Skule/NTNU - Dataingeniør/6-semester/Bachelor/Projects/PHP/lumen_test/application');
    header('Content-Type: application/x-yaml');
    echo $openapi->toYaml();
});

#LDAP

// $router->get('/users/person', function () {
//     $controller = $app->make('\App\Http\Controllers\LdapController');
//     return $controller->person('ematli', true);
// });