FROM php:7.2-cli

RUN apt-get update && apt-get install -y \
        unzip \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libldap2-dev\
        libpng-dev \
        libaio1
RUN     rm -rf /var/lib/apt/lists/* && \
        docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
        docker-php-ext-install ldap

# Oracle instantclient
COPY instantclient-basic-linux.x64-12.2.0.1.0.zip /opt/oracle/
COPY instantclient-sdk-linux.x64-12.2.0.1.0.zip /opt/oracle/

RUN unzip /opt/oracle/instantclient-basic-linux.x64-12.2.0.1.0.zip -d /usr/local/
RUN unzip /opt/oracle/instantclient-sdk-linux.x64-12.2.0.1.0.zip -d /usr/local/

RUN ln -s /usr/local/instantclient_12_2 /usr/local/instantclient
RUN ln -s /usr/local/instantclient/libclntsh.so.12.1 /usr/local/instantclient/libclntsh.so

ENV LD_LIBRARY_PATH="/usr/local/instantclient_12_2/"

RUN echo 'instantclient,/usr/local/instantclient' | pecl install oci8
RUN echo "extension=oci8.so" > /usr/local/etc/php/conf.d/php-oci8.ini

# Install Composer in order to install Laravel-OCI8 extension
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /opt/backend/
WORKDIR /opt/backend/

RUN composer update

RUN chmod -R 777 /opt/backend/storage

# Generate OpenAPI documentation
RUN php artisan swagger-lume:publish
RUN php artisan swagger-lume:generate


EXPOSE 8000

CMD php -S 0.0.0.0:8000 public/index.php